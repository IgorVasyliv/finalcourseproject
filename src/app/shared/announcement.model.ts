export interface Announcement {
     action: string;
     currency: string;
     amount: number;
      id?: number;
}

