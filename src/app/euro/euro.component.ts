import {Component, Injectable, OnInit} from '@angular/core';
import {EURO, TOBUY, TOSELL} from '../shared/const';
import {FireBaseApiService} from '../shared/service/firebase.service';
import {Announcement} from '../shared/announcement.model';
import {ErrorsHandler} from '../shared/errors.handler';

@Component({
  selector: 'app-buy',
  templateUrl: './euro.component.html',
  styleUrls: ['./euro.component.css']
})

@Injectable()
export class EuroComponent implements OnInit {

  Announcements: Array<Announcement>;
  isSellSelected;
  isBuySelected;

  constructor(private firebaseAPI: FireBaseApiService, private ErrorsHandle: ErrorsHandler) {
    this.firebaseAPI.loadData().subscribe(
      (data) => {
        this.isSellSelected = true;
        return this.Announcements = data.filter(item => item.action === TOSELL && item.currency === EURO);
      },
      (error) => this.ErrorsHandle.handleError(error),
    );
  }

  ngOnInit() {
  }

  getAnnouncements(action) {
    this.firebaseAPI.loadData().subscribe(
      (data) => {
        if (action === TOBUY) {
          this.isBuySelected = true;
          this.isSellSelected = false;
        } else {
          this.isBuySelected = false;
          this.isSellSelected = true;
        }
        this.Announcements = data.filter(item => item.action === action && item.currency === EURO);
      },
      (error) => this.ErrorsHandle.handleError(error),
    );
  }

}
