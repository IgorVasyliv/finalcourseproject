import {Injectable} from '@angular/core';
import {Observable, Observer} from 'rxjs/index';
import {Announcement} from '../announcement.model';
import {fromPromise} from 'rxjs/internal/observable/fromPromise';


declare var firebase: any;

@Injectable()
export class FireBaseApiService {
  config: any = {
    apiKey: 'AIzaSyBXjpjVjHnVWRsbGY7zj01WdWsm74qJL-8',
    authDomain: 'ivangularapp.firebaseapp.com',
    databaseURL: 'https://ivangularapp.firebaseio.com',
    projectId: 'ivangularapp',
    storageBucket: 'ivangularapp.appspot.com',
    messagingSenderId: '622365509562'
  };
  database: any;
  DBdata: Array<Announcement> = [];

  constructor() {
  }

  initDB() {
    firebase.initializeApp(this.config);

    firebase.auth().signInAnonymously()
      .catch(function (error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;

        console.log(`Auth error. errorCode - ${errorCode}, errorMessage - ${errorMessage}`);
      });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.database = firebase.database();
        this.loadData();
      }
    });
  }

  loadData(): Observable<Announcement[]> {
    let data = new Observable<Announcement[]>((observer: Observer<Announcement[]>) => {
      const tobuys = firebase.database().ref('tobuy');
      tobuys.on('value', (Announcement) => {
        this.DBdata = [];
        Announcement.forEach(announcement => {
          this.DBdata.push(announcement.val());
        });
        observer.next(this.DBdata);
      });
    });
    return (data);
  }

  add(announcement) {
    const newId = announcement.id;
    return fromPromise(firebase.database().ref('tobuy/test' + newId).set(announcement));
  }

  remove(id: number) {
    return fromPromise(firebase.database().ref('tobuy/test' + id).remove());
  }
}
