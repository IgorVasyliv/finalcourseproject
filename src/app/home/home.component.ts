import {Component, Injectable, OnInit} from '@angular/core';
import {Announcement} from '../shared/announcement.model';
import {SpeechRecognition} from '../speech-recognition.service';
import {FireBaseApiService} from '../shared/service/firebase.service';
import {TOBUY, TOSELL} from '../shared/const';
import {ErrorsHandler} from '../shared/errors.handler';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

@Injectable()
export class HomeComponent implements OnInit {
  Announcements: Array<Announcement>;
  stringArray = [];
  isSellSelected;
  isBuySelected;

  constructor(private speech: SpeechRecognition,
              private firebaseAPI: FireBaseApiService,
              private ErrorsHandle: ErrorsHandler) {
    this.firebaseAPI.loadData().subscribe(
      (data) => {
        this.isSellSelected = true;
        return this.Announcements = data.filter(item => item.action === TOSELL);
      },
      (error) => this.ErrorsHandle.handleError(error),
    );
  }

  ngOnInit() {
    this.speech.init();
  }

  getAnnouncements(action) {
    this.firebaseAPI.loadData().subscribe(
      (data) => {
        if (action === TOBUY) {
          this.isBuySelected = true;
          this.isSellSelected = false;
        } else {
          this.isBuySelected = false;
          this.isSellSelected = true;
        }
        this.Announcements = data.filter(item => item.action === action);
      },
      (error) => this.ErrorsHandle.handleError(error),
    );
  }

  onParse() {
    this.speech.start();
    this.speech.setSpeechParseResultListener((command) => {
      this.speech.stop();
      this.stringArray = command.split(' ');
      const newId = +(new Date());
      const newAnnouncement: Announcement = {
        action: this.stringArray[0],
        currency: this.stringArray[1],
        amount: parseInt(this.stringArray[2]),
        id: newId
      };
      this.firebaseAPI.add(newAnnouncement).subscribe(
        (result) => console.log('success'),
        (error) => this.ErrorsHandle.handleError(error),
      );
    });
  }

  remove(id: number) {
    this.firebaseAPI.remove(id).subscribe(
      (result) => console.log('success'),
      (error) => this.ErrorsHandle.handleError(error),
    );
  }
}
