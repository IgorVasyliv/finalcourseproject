import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SpeechRecognition} from './speech-recognition.service';
import {FireBaseApiService} from './shared/service/firebase.service';
import {ErrorsHandler} from './shared/errors.handler';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatMenuModule,
    MatToolbarModule,
  ],
  providers: [
    SpeechRecognition,
    FireBaseApiService,
    {
      provide: ErrorsHandler, useClass: ErrorsHandler,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
