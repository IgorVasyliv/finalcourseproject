import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EuroRoutingModule} from './euro-routing.module';
import {EuroComponent} from './euro.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    EuroRoutingModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
  ],
  declarations: [EuroComponent]
})
export class EuroModule {
}
