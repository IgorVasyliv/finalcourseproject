import {Component} from '@angular/core';
import {FireBaseApiService} from './shared/service/firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private firebase: FireBaseApiService) {
    firebase.initDB();
  }
}


