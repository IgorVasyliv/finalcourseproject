import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DollarsComponent} from './dollars.component';

const routes: Routes = [
  {
    path: '',
    component: DollarsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DollarsRoutingModule {
}
