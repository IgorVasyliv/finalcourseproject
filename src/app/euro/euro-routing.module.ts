import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EuroComponent} from './euro.component';

const routes: Routes = [
  {
    path: '',
    component: EuroComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EuroRoutingModule { }
