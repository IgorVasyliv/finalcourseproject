import {Component, Injectable, OnInit} from '@angular/core';
import {Announcement} from '../shared/announcement.model';
import {FireBaseApiService} from '../shared/service/firebase.service';
import {DOLLARS, TOBUY, TOSELL} from '../shared/const';
import {ErrorsHandler} from '../shared/errors.handler';

@Component({
  selector: 'app-sell',
  templateUrl: './dollars.component.html',
  styleUrls: ['./dollars.component.css'],
})

@Injectable()
export class DollarsComponent implements OnInit {
  Announcements: Array<Announcement>;
  isSellSelected;
  isBuySelected;

  constructor(private firebaseAPI: FireBaseApiService, private ErrorsHandle: ErrorsHandler) {
    this.firebaseAPI.loadData().subscribe(
      (data) => {
        this.isSellSelected = true;
        return this.Announcements = data.filter(item => item.action === TOSELL && item.currency === DOLLARS);
      },
      (error) => this.ErrorsHandle.handleError(error),
    );
  }

  ngOnInit() {

  }

  getAnnouncements(action) {
    this.firebaseAPI.loadData().subscribe(
      (data) => {
        if (action === TOBUY) {
          this.isBuySelected = true;
          this.isSellSelected = false;
        } else {
          this.isBuySelected = false;
          this.isSellSelected = true;
        }
        this.Announcements = data.filter(item => item.action === action && item.currency === DOLLARS);
      },
      (error) => this.ErrorsHandle.handleError(error),
    );
  }
}










