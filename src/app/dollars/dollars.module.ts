import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {DollarsRoutingModule} from './dollars-routing.module';
import {DollarsComponent} from './dollars.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    DollarsRoutingModule,
    MatDividerModule,
    MatListModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule
  ],
  declarations: [DollarsComponent]
})
export class DollarsModule {
}
