import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {FormsModule} from '@angular/forms';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      HomeRoutingModule,
      MatButtonToggleModule,
      MatButtonModule,
      MatIconModule
    ],
    declarations: [HomeComponent],
  }
)
export class HomeModule {
}
